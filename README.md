# Módulo HTTP
Es un módulo fundamental para implementar aplicaciones en un servidor web.

## Protocolo HTTP

**HTTP** (protocolo de transferencia de hipertexto) permite la transferencia de datos entre un servidor web un navegador.

http://host[:puerto][/ruta y archivo][?parámetros]

* __http__, indica el protocolo que utilizamos para conectarnos.
* __host__, nombre del dominio.
* __puerto__, el protocolo http utiliza el número 80 por defecto.
* __[/ruta y archivo]__, indica la ruta del archivo en el directorio del servidor.
* __[?parémtros]__, son datos que se pueden enviar desde el host al servidor, para identificar con exactitud el recurso.

## Servidor web con Node.js

> Con Node.js somo nosotros los que codificamos el servidor web. Utilizando el módulo 'http'.
> Tras la ejecución (node servidor.js, en el navegador colocamos la dirección IP seguido del número de puerto, en nuestro caso :8888. Ello mostrará el contenido de la página web creada.)

## Protocolo MIME
> Los MIME TYPES son la manera estándar de mandar contenido a través de internet, especificando a cada archivo con su tipo de contenido.
> Según el tipo de archivo que retornamos indicamos un valor distinto a Content-Type:
> Listado completo MIME: https://www.sitepoint.com/mime-types-complete-list/
# P2 - SERVIDOR WEB DE ARCHIVOS ESTÁTICOS (png, mp3, etc.)
> Corresponde con la UT-1 del módulo IUN.

# P3 - SERVIDOR WEB DE ARCHIVOS Y CREACIÓN DE UNA CACHÉ
> La mecánica empleada, es que, en cada solicitud del cliente (navegador) analizamos si dicho recurso se encuentra en el servidor y en caso afirmativo procedemos a leerlo mediante las funciones que provee el módulo 'fs'. Retornamos al navegador el recurso indicado.

> La modificación al programa, es para reducir el tiempo requerido de acceso al disco duro del servidor, donde están almacenados los archivos.

> Lo que implementamos es un sistema de cache en el servidor para almacenar el contenido de los archivos estáticos - aquellos que no cambian - y tenerlos almacenados en la memoria RAN. El sistema se hace más eficiente.

# P4 - RECUPERAR DATOS DE UN FORMULARIO HTML
> Programa que puede procesar los datos de un formulario HTML. Al presionar el botón submit se procede a recuperar los datos de un formulario y generar una página HTML dinámica mostrando los valores.