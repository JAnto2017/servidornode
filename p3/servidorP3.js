const http = require('http');
const fs = require('fs');

const mime = {
    'html'  :   'text/html',
    'css'   :   'text/css' ,
    'jpg'   :   'image/jpg',
    'ico'   :   'image/x-icon',
    'mp3'   :   'audio/mpeg3',
    'mp4'   :   'video/mp4'
}

const cache = {}

const servidor = http.createServer((pedido, respuesta) => {
    const url = new URL('http://localhost:7799' + pedido.url);
    let camino = 'static' + url.pathname;

    if (camino == 'static') {
        camino ='static/index_u2.html';
    }

    if (cache[camino]) {
        const vec = camino.split('.');
        const extension = vec[vec.length - 1];
        const mimearchivo = mime[extension];
        respuesta.writeHead(200, {'Content-Type': mimearchivo});
        respuesta.write(cache[camino]);
        respuesta.end();
        console.log('Recurso recuperado del cache: ' + camino);
    } else {
        fs.stat(camino, error => {
            if (!error) {
                fs.readFile(camino, (error,contenido) => {
                    if (error) {
                        respuesta.writeHead(500, {'Content-Type': 'text/html'});
                        respuesta.write('<!DOCTYPE html><html><head></head><body><h1>Error 500</h1><h2>Error interno</h2></body></html>');
                        respuesta.end();
                    } else {
                        cache[camino] = contenido;
                        const vec = camino.split('.');
                        const extension = vec[vec.length - 1];
                        const mimearchivo = mime[extension];
                        respuesta.writeHead(200, {'Content-Type': mimearchivo});
                        respuesta.write(contenido);
                        respuesta.end();
                        console.log('Recurso leido del disco: ' + camino);
                    }
                });
            } else {
                respuesta.writeHead(404, {'Content-Type': 'text/html'});
                respuesta.write('<!DOCTYPE html><html><head></head><body><h1>Error 404</h1><h2>Recurso inexistente</h2></body></html>');
                respuesta.end();
            }
        });
    }
});

servidor.listen(7799);
console.log('Servidor web iniciado');

/**
 * Definimos un objeto vacío: const cache = {}
 * Cuando se dispara un pedido de recursos y se ejecuta la función anónima que le pasamos al método createServer. Mediante un if, verificamos si el objeto almacena una propiedad que coincide con el camino del recurso.
 * Luego de leido, almacenamos el contenido del recurso en la variable 'cache' y como valor de propiedad indicamos el camino del recurso. Esto hace que quede en la variable caché y como valor de propiedad indicamos el camino del recurso: cache[camino] = contenido.
 */