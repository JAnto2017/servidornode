const http = require('http');

const servidor = http.createServer((pedido,respuesta) => {
    respuesta.writeHead(200, {'Content-Type':'text/html'});
    respuesta.write(`<!DOCTYPE html><html><head></head><body><h1>Servidor Web Node.js</h1></body></html>`);
    respuesta.end();
});

servidor.listen(8888);

console.log('Servidor web iniciado');